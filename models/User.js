const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  email: {
    type: String, 
    required: [true, "Email is required"]
  },
  password: {
    type: String,
    required: [true, "Password is required"]
  }, 
  isAdmin: {
    type: Boolean, 
    default: false
  },
  orders: [
  {
    products: [
    {
      productId: {
        type: String, 
        required: [true, "Product Id is required"]
      },
      quantity: {
        type: Number, 
        default: [true, "Quantity is required"]
      }
    }],
    totalAmount: {
      type: Number, 
      default: 500
    },
    purchasedOn: {
      type: Date, 
      default: new Date ()
    }
  }]
})

module.exports = mongoose.model("User", userSchema);
