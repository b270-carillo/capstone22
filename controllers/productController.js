const Product= require("../models/Product");
const auth = require("../auth");

// Create Product
module.exports.createProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	let newProduct = new Product({
		name: req.body.name, 
		description: req.body.description,
		price: req.body.price
	})

	if(userData.isAdmin == true){
		return newProduct.save().then(course => {
			console.log(course)
			res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	} else {
		res.status(403).send("Forbidden: Needs admin rights")
	}
	
}

// Get all products
module.exports.getAllProducts = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        return Product.find({}).then(result => res.send(result));
    } else {
        return res.send(false);
    }

}

// Get all active products
module.exports.getAllActive = (req, res) => {

    return Product.find({isActive: true}).then(result => res.send(result));
}

// Get single product
module.exports.getProduct = (req, res) => {

    console.log(req.params.productId)

    return Product.findById(req.params.productId).then(result => {
    	console.log(result);
    	return res.send(result);
    })
    .catch(error => {
    	console.log(error);
    	return res.send(error)
    })
}

// update details of product
module.exports.updateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let updateProduct = {
			name: req.body.name, 
			description: req.body.description,
			price: req.body.price
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false)
	}
}

// For archiving a product
module.exports.archiveProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		if(userData.isAdmin) {
			let updateActivity = {
				isActive: req.body.isActive
			}
			
			return Product.findByIdAndUpdate(req.params.productId, updateActivity).then(result => {
				console.log(result);
				res.send(true);
			})
			.catch(error => {
				console.log(error);
				res.send(false);
			})
		} else {
			return res.send(false)
		}
	}
