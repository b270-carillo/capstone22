const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// user registration
module.exports.registerUser = (req, res) => {
	let newUser = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
	})

	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}


// user authentication
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		if(result == null) {

			return res.send({message: "No user found"});

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(result)});

			} else {
				return res.send(false)
			}
		}
	})
};

// get user details
module.exports.getProfile = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

	return res.send(result);
	});

};

// checkout order
module.exports.checkout = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin) {
		let productName = await Product.findById(req.body.productId).then(result => result.name);

		let data = {
			userId: userData.id,
			email: userData.email,
			productId: req.body.productId,
		}
		console.log(data);

		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.orders.push({
				productId: data.productId,
			});

			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);

		let isProductUpdated = await Product.findById(data.productId)
		.then(product => {

			product.orders.push({
				userId: data.userId
			})


			return product.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isProductUpdated);

		if(isUserUpdated && isProductUpdated) {
			return res.send(true);

		} else {
			return res.send(false);
		}
	} else {
		return res.send(false);
	}

}








///////////// Stretch goals /////////////

//
module.exports.makeAdmin = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let updateUser = {
			email: req.body.email, 
			isAdmin: req.body.isAdmin
		}
		//{new:true} = returns the updated document
		return User.findByIdAndUpdate(req.params.userId, updateUser, {new:true}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false)
	}
}
