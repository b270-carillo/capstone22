const express = require("express");
const router = express.Router()
const userController = require("../controllers/userController")
const auth = require("../auth")



router.post("/register", userController.registerUser)

router.post("/login", userController.loginUser);

router.get("/details", auth.verify, userController.getProfile)

router.post("/checkout", auth.verify, userController.checkout);

router.put("/:userId/makeAdmin", auth.verify, userController.makeAdmin);



module.exports = router;
