const express = require("express");
const router = express.Router()
const productController = require("../controllers/productController")
const auth = require("../auth")

// Create a Product
router.post("/", auth.verify, productController.createProduct)

// Retrieve all courses (admin)
router.get("/allProducts", auth.verify, productController.getAllProducts)

// Retrieve all active courses
router.get("/", productController.getAllActive)

// Retrieve one course
router.get("/:productId", productController.getProduct)

// Update product information
router.put("/:productId", auth.verify, productController.updateProduct)

// Route for archiving a product
router.patch("/:productId/archive", auth.verify, productController.archiveProduct)

module.exports = router;
